class epicsgateway (
    String $service_name,
    Boolean $service_enable,
    String $service_ensure,
    String $package_ensure,
    String $package_name,
    String $access_file_source,
    Hash $instance_settings,
) {
    package { $epicsgateway::package_name:
	ensure => $epicsgateway::package_ensure,
    }

    if $access_file_source {
	file {'/etc/epicsgw/epicsgw.access':
	    ensure => 'file',
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    require => Package["$package_name"],
	    source => $access_file_source,
	}
    } else {
	file {'/etc/epicsgw/epicsgw.access':
	    ensure => 'absent',
	}
    }

    $instance_settings.each |String $instance_name, Hash $values| {
	service {"epicsgateway-${instance_name}":
	    name => "${epicsgateway::service_name}@${instance_name}",
	    enable => $epicsgateway::service_enable,
	    ensure => $epicsgateway::service_ensure,
	    require => Package["$::epicsgateway::package_name"],
	    subscribe => File['/etc/epicsgw/epicsgw.access'],
	}

	file {"/etc/sysconfig/epicsgateway-${instance_name}":
	    ensure => 'file',
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    content => epp('epicsgateway/epicsgateway.sysconfig.epp', {
		'epics_ca_addr_list' => $values[epics_ca_addr_list],
		'epics_ca_auto_addr_list' => $values[epics_ca_auto_addr_list],
		'epics_cas_intf_addr_list' => $values[epics_cas_intf_addr_list],
		'epics_cas_ignore_addr_list' => $values[epics_cas_ignore_addr_list],
		'epics_prefix' => $values[epics_prefix],
	    }),
	    notify => Service["epicsgateway-${instance_name}"],
	}
    }

    systemd::manage_dropin {'restart.conf':
        ensure => 'present',
        unit   => 'epicsgateway@.service',
        service_entry => {
            'Restart' => 'on-failure',
            'RestartSec' => '15s',
        },
    }
    systemd::manage_dropin {'core.conf':
        ensure => 'present',
        unit   => 'epicsgateway@.service',
        service_entry => {
            'LimitCORE' => 'infinity',
        },
    }
}
